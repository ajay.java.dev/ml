# Machine Learning Algorithms
Following are the links used for learning ML. Since Python, Scala, R and Java are
being used at different places for computing ML algorithms on datasets, this repo 
has combinations for all.

| source | link |
| ------ | ------ |
| Linkedln | https://www.linkedin.com/learning/paths/advance-your-skills-as-an-apache-spark-specialist |
| Coursera(MCS-DS) | https://www.coursera.org/learn/cs-498/programming/DnvKF/machine-problem-5-spark-graphframes-and-mllib | 




![Alt text](img/stnd_1.PNG?width=70 "Standardization of feature set")